<?php
/**
 * DynamoDBと接続するクラス
 */
require_once dirname(__FILE__).'/../vendor/autoload.php';
use Aws\DynamoDb\DynamoDbClient;

class DynamoDBConnection
{
    private $config;
    private $client;

    public function __construct($config)
    {
        $this->config = $config;
        $this->client = DynamoDbClient::factory($this->config);
    }

    public function createTable($table)
    {
        $schema = array(
            'TableName' => $table,
            'AttributeDefinitions' => array(
                array(
                    'AttributeName' => 'user_id',
                    'AttributeType' => 'N'
                ),
                array(
                    'AttributeName' => 'customer_id',
                    'AttributeType' => 'N'
                )
            ),
            'KeySchema' => array(
                array(
                    'AttributeName' => 'user_id',
                    'KeyType'       => 'HASH'
                ),
                array(
                    'AttributeName' => 'customer_id',
                    'KeyType'       => 'RANGE'
                )
            ),
            'ProvisionedThroughput' => array(
                'ReadCapacityUnits'  => 10,
                'WriteCapacityUnits' => 10
            )
        );

        $this->client->createTable($schema);
    }

    public function describeTable($table)
    {
        echo $this->client->describeTable(array(
            'TableName' => $table,
        ));
    }
}

?>
