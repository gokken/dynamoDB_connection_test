<?php
require_once dirname(__FILE__) . '/lib/DynamoDBConnection.php';
// 設定ファイル読み込み
$jsonFile = file_get_contents(dirname(__FILE__).'/config/config.json');
$json_encoding = mb_convert_encoding($jsonFile,'UTF8','ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
$connection_info = json_decode($json_encoding,true);

$dynamo = new DynamoDBConnection($connection_info);

// テーブルを作成
$dynamo->createTable('test');

// テーブルを表示
$dynamo->describeTable('test');