## DynamoDBLocalを試すテストリポジトリ
このリポジトリは、dynamoDBLocalの立ち上げから、testテーブルの作成、表示までするリポジトリです。

### Dockerコンテナに関して
 - appコンテナのディストリビューション  
 Debian GNU/Linux 8

 - 仮想環境の立ち上げ
 
```
$ docker-compose up --build
$ docker-compose up -d
```

### Usage

 - web/config/config.sample.json -> web/config/config.jsonに変更の後、
 
 ```json:config.json
{
    "region"  : "リージョンの設定((例) ap-northeast-1)",
    "version" : "現在のバージョン((例) latest)",
    "endpoint": "アクセスを送るエンドポイントの設定((例) http://{dynamoDBLocalのコンテナのipアドレス}:8080)",
    "key"     : "シークレットキー((例) YOUR_KEY)",
    "secret"  : "シークレットID((例) YOUR_KEY)"
}
```

を記述

### Directory
```
├── docker-compose.yml
├── dynamoDB
│   └── Dockerfile
└── web
    ├── Dockerfile
    ├── composer.json
    ├── composer.lock
    ├── config
    │   ├── config.json # 設定ファイル
    │   └── php.ini
    ├── index.php #ここで処理を行っていきます
    └── lib
        └── DynamoDBConnection.php # DynamoDBLocalに対する処理を記述するクラス
       
```

### Requirement
 - composer
 - docker
 - docker-compose
 - php
